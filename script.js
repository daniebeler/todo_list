const tasks = [];

printTasks();

document.getElementById("addTask").addEventListener("click", function(){
    let taskcontent = document.getElementById("inputtask").value;
    let taskresponsible = document.getElementById("inputresponsible").value;
    let newTask = {name: taskcontent, responsible:taskresponsible, done: false}
    tasks.push(newTask);
    printTasks();
})

function printTasks(){
    document.getElementById("tasklist").innerHTML = getHTMLOfList()
}

function getHTMLOfList(){
    let html = "";
    let index = 0;
    tasks.forEach(Element =>{
        let checked = "";
        if(Element.done){
            checked = "checked";
        }
        
        html += "<li><input onclick='markTask(this)' data-index='" + index + "' type='checkbox'" + checked + "/>" + Element.name + " - " + Element.responsible + "</li>";
        index++;
    });
    return html;
}

function markTask(Element){
    let index = Element.attributes["data-index"].value;
    let isChecked = Element.checked;
    tasks[index].done = isChecked;
    printTasks();
}